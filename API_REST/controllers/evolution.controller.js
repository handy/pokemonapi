const models = require('../models/index')
exports.test = (req, res) => {
    res.send('Greetings from the Test controller!');
};
exports.evolution_details = (req,res) => {
    models.Evolutions.findByPk(req.params.id, {
        include: [{
          model: models.EvolutionObjects,
        }]
      }).then(evolution => {
        res.json(evolution)
    }).catch((e) => {
        console.log(e)
    })
}

exports.evolution_all = (req, res) => {
models.Evolutions.findAll({
    include: [{
      model: models.EvolutionObjects,
    }]
  }).then(evolutions => {
        res.json(evolutions)
    }).catch((e) => {
        console.log(e);
    });
};