'use strict';
module.exports = (sequelize, DataTypes) => {
  const Evolutions = sequelize.define('Evolutions', {
    EvolutionCost: DataTypes.INTEGER
  }, {});
  Evolutions.associate = function(models) {
    Evolutions.belongsTo(models.EvolutionObjects)
  };
  return Evolutions;
};