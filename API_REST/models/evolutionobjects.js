'use strict';
module.exports = (sequelize, DataTypes) => {
  const EvolutionObjects = sequelize.define('EvolutionObjects', {
    EvolutionObjectName: DataTypes.STRING
  }, {});
  EvolutionObjects.associate = function(models) {
    // associations can be defined here
  };
  return EvolutionObjects;
};