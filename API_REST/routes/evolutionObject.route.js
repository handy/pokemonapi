const express = require('express');
const router = express.Router();
const evolutionObject_controller = require('../controllers/evolutionObject.controller');


router.get('/test', evolutionObject_controller.test);
router.get('/:id', evolutionObject_controller.evolutionObject_details);
router.get('/', evolutionObject_controller.evolutionObject_all)
module.exports = router;