const express = require('express');
const router = express.Router();
const evolution_controller = require('../controllers/evolution.controller');


router.get('/test', evolution_controller.test);
router.get('/:id', evolution_controller.evolution_details);
router.get('/', evolution_controller.evolution_all)
module.exports = router;