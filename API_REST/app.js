// app.js
const express = require('express');
const bodyParser = require('body-parser');
const evolution = require('./routes/evolution.route.js');
const evolutionObject = require('./routes/evolutionObject.route.js');
const db = require('./database');
const Sequelize = require('sequelize');

// initialize our express app
const app = express();
let port = 3030;

//connecting to database
const sequelize = new Sequelize(db.name, db.username, db.password, {
    host: 'localhost',
    dialect: db.dialect
  });

//setting up body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/evolution', evolution);
app.use('/evolutionObject', evolutionObject)

//starting the app
app.listen(port, () => {
    console.log('Server is up and running on port number ' + port);
});